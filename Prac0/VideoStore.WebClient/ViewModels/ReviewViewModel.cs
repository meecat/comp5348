﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VideoStore.Services.MessageTypes;

namespace VideoStore.WebClient.ViewModels
{
    public class ReviewViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required] 
        [DataType(DataType.Text)]
        [Display(Name = "Rating")]
        [Range(0,5)]
        public int Rating { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Review Text")]
        public string ReviewText { get; set; } 
    }
}