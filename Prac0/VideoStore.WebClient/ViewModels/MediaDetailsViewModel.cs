﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoStore.Services.MessageTypes;

namespace VideoStore.WebClient.ViewModels
{
    public class MediaDetailsViewModel
    {
        public Media Media { get; set; }
        public List<Review> Reviews { get; set; }

        public MediaDetailsViewModel()
        {
            Reviews = new List<Review>();
        }
    }
}
