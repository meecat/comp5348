﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VideoStore.Services.MessageTypes;
using VideoStore.WebClient.ViewModels;

namespace VideoStore.WebClient.Controllers
{ 
    public class MediaController : ApiController
    { 
        public List<Media> Get()
        {
            return new CatalogueViewModel().Items;
        }
    }
}
