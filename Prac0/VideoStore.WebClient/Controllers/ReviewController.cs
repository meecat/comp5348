﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VideoStore.Services.MessageTypes;
using VideoStore.WebClient.ViewModels;

namespace VideoStore.WebClient.Controllers
{
    public class ReviewController : Controller
    {
        public ActionResult Index(int mediaId)
        {
            var mediaDetialsViewModel = new MediaDetailsViewModel
            {
                Media = ServiceFactory.Instance.CatalogueService.GetMediaById(mediaId),
                Reviews = ServiceFactory.Instance.ReviewService.GetReviewItems(mediaId, 0, int.MaxValue)
            };

            return View(mediaDetialsViewModel);
        }

        public ActionResult CreateReview(int mediaId)
        {
            ViewBag.MediaId = mediaId;

            return View();
        }

        [HttpPost]
        public ActionResult CreateReview(int mediaId, ReviewViewModel pReview, UserCache pUser)
        {
            var author = new ReviewAuthor
            {
                Id = pUser.Model.Id,
                Name = pUser.Model.Name,
                City = pUser.Model.City,
                Country = pUser.Model.Country
            };

            var review = new Review
            {
                Title = pReview.Title,
                Date = DateTime.Now,
                Rating = pReview.Rating,
                ReviewText = pReview.ReviewText, 
            };

            ServiceFactory.Instance.ReviewService.CreateReview(mediaId, review, author);

            return RedirectToAction("Index", new { mediaId });
        }
    }
}
