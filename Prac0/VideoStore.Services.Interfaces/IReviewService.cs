﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Services.Interfaces
{
    [ServiceContract]
    public interface IReviewService
    {
        [OperationContract] 
        void CreateReview(int mediaId, Review review, ReviewAuthor author);

        [OperationContract]
        List<Review> GetReviewItems(int mediaId, int offset, int count);
    }
}
