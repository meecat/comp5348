﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Services.Interfaces;
using VideoStore.Services.MessageTypes;

namespace VideoStore.Services
{
    public class ReviewService : IReviewService
    {
        private IReviewProvider ReviewProvider
        {
            get
            {
                return ServiceFactory.GetService<IReviewProvider>();
            }
        }

        private ICatalogueProvider CatalogueProvider
        {
            get
            {
                return ServiceFactory.GetService<ICatalogueProvider>();
            }
        }

        public void CreateReview(int mediaId, Review review, ReviewAuthor author)
        {
            var internalReview = MessageTypeConverter.Instance.Convert<
                VideoStore.Services.MessageTypes.Review,
                VideoStore.Business.Entities.Review>(review);

            var internalUser = MessageTypeConverter.Instance.Convert<
                VideoStore.Services.MessageTypes.ReviewAuthor,
                VideoStore.Business.Entities.User>(author);

            internalReview.User = internalUser;
            internalReview.Medium = CatalogueProvider.GetMediaById(mediaId);

            ReviewProvider.CreateReview(internalReview);
        }

        public List<Review> GetReviewItems(int mediaId, int offset, int count)
        {
            var externalResult = MessageTypeConverter.Instance.Convert<
                List<VideoStore.Business.Entities.Review>,
                List<VideoStore.Services.MessageTypes.Review>>(
                ReviewProvider.GetReviewItems(mediaId, offset, count));

            return externalResult;
        }
    }
}
