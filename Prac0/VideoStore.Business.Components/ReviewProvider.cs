﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;

namespace VideoStore.Business.Components
{
    public class ReviewProvider : IReviewProvider
    {
        public List<Review> GetReviewItems(int mediaId, int offset, int count)
        {
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                var reviewItems = (from review in lContainer.Reviews.Include("Medium")
                                   where review.Medium.Id == mediaId
                                   select review)
                                   .OrderByDescending(x => x.Date)
                                   .Skip(offset)
                                   .Take(count)
                                   .ToList();

                return reviewItems;
            }
        }

        public void CreateReview(Review review)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                lContainer.Users.Attach(review.User);
                lContainer.Media.Attach(review.Medium);

                //update avarage rating
                review.Medium.RatingsSum += review.Rating;
                review.Medium.RatingsCount++;
                review.Medium.AverageRating = decimal.Round(review.Medium.RatingsSum / review.Medium.RatingsCount, 2);

                lContainer.Reviews.Add(review);


                lContainer.SaveChanges();
                lScope.Complete();
            }
        }
    }
}
