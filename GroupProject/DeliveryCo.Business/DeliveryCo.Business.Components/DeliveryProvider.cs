﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliveryCo.Business.Components.Interfaces;
using System.Transactions;
using DeliveryCo.Business.Entities;
using System.Threading;
using DeliveryCo.Services.Interfaces;
using System.Threading.Tasks;

namespace DeliveryCo.Business.Components
{
    public class DeliveryProvider : IDeliveryProvider
    {
        public Guid SubmitDelivery(DeliveryInfo pDeliveryInfo, int orderId)
        {
           // using (TransactionScope lScope = new TransactionScope())
            using (DeliveryDataModelContainer lContainer = new DeliveryDataModelContainer())
            {
                pDeliveryInfo.DeliveryIdentifier = Guid.NewGuid();
                pDeliveryInfo.Status = 0;
                lContainer.DeliveryInfoes.AddObject(pDeliveryInfo);
                lContainer.SaveChanges();
                // ThreadPool.QueueUserWorkItem(new WaitCallback((pObj) => ScheduleDelivery(pDeliveryInfo, orderId)));

                //lScope.Complete();
            }

            Task.Run(() => ScheduleDelivery(pDeliveryInfo, orderId));

            return pDeliveryInfo.DeliveryIdentifier;
        }

        private void ScheduleDelivery(DeliveryInfo pDeliveryInfo, int orderId)
        {
            Console.WriteLine("Delivering to " + pDeliveryInfo.DestinationAddress);
            Thread.Sleep(3000);
            //notifying of delivery completion
            //using (TransactionScope lScope = new TransactionScope())
            using (DeliveryDataModelContainer lContainer = new DeliveryDataModelContainer())
            {
                lContainer.Attach(pDeliveryInfo);
                pDeliveryInfo.Status = 1;
                lContainer.SaveChanges();
            }

            //IDeliveryNotificationService lService = DeliveryNotificationServiceFactory.GetDeliveryNotificationService(pDeliveryInfo.DeliveryNotificationAddress);
            IDeliveryNotificationService lService = MessageBus.MessageServiceFactory.GetService<IDeliveryNotificationService>("DeliveryNotificationTransacted");

            lService.NotifyDeliveryCompletion(pDeliveryInfo.DeliveryIdentifier, orderId, DeliveryInfoStatus.Delivered);


        }
    }
}
