﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;
using Microsoft.Practices.ServiceLocation;
using System.Transactions; 

namespace VideoStore.Business.Components
{
    public class DeliveryNotificationProvider : IDeliveryNotificationProvider
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public void NotifyDeliveryCompletion(Guid pDeliveryId, int orderId, DeliveryStatus status)
        {
            //Order lAffectedOrder = RetrieveDeliveryOrder(pDeliveryId);
            Order lAffectedOrder = RetrieveOrder(orderId);
            UpdateDeliveryStatus(pDeliveryId, status);
            if (status == Entities.DeliveryStatus.Delivered)
            {
                EmailProvider.SendMessage(new EmailMessage()
                {
                    ToAddress = lAffectedOrder.Customer.Email,
                    Message = "Our records show that your order" +lAffectedOrder.OrderNumber + " has been delivered. Thank you for shopping at video store"
                });
            }
            if (status == Entities.DeliveryStatus.Failed)
            {
                EmailProvider.SendMessage(new EmailMessage()
                {
                    ToAddress = lAffectedOrder.Customer.Email,
                    Message = "Our records show that there was a problem" + lAffectedOrder.OrderNumber + " delivering your order. Please contact Video Store"
                });
            }

            OrderCompletion(pDeliveryId, orderId);
        }


        private void OrderCompletion(Guid deliveryIdentifier, int orderId)
        {
            using (TransactionScope lScope = new TransactionScope())
            {
                using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
                {
                    var order = lContainer.Orders.Include("Customer").Where(x => x.Id == orderId).FirstOrDefault();
                    try
                    {
                        Delivery lDelivery = new Delivery() { DeliveryStatus = DeliveryStatus.Submitted, SourceAddress = "Video Store Address", DestinationAddress = order.Customer.Address, Order = order };

                        lDelivery.ExternalDeliveryIdentifier = deliveryIdentifier;

                        order.Delivery = lDelivery;
                        lContainer.Orders.ApplyChanges(order);

                        lContainer.SaveChanges();

                        SendOrderPlacedConfirmation(order);

                        lScope.Complete();
                    }
                    catch (Exception lException)
                    {
                        SendOrderErrorMessage(order, lException);
                        throw;
                    }
                }
            }
        }


        private void SendOrderErrorMessage(Order pOrder, Exception pException)
        {
            //bruce: publish error message

            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "There was an error in processsing your order " + pOrder.OrderNumber + ": " + pException.Message + ". Please contact Video Store"
            });
        }

        private void SendOrderPlacedConfirmation(Order pOrder)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "Your order " + pOrder.OrderNumber + " has been placed"
            });
        }



        private void UpdateDeliveryStatus(Guid pDeliveryId, DeliveryStatus status)
        {
            using (TransactionScope lScope = new TransactionScope())
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Delivery lDelivery = lContainer.Deliveries.Where((pDel) => pDel.ExternalDeliveryIdentifier == pDeliveryId).FirstOrDefault();
                if (lDelivery != null)
                {
                    lDelivery.DeliveryStatus = status;
                    lContainer.SaveChanges();
                }
                lScope.Complete();
            }
        }

        /*
        private Order RetrieveDeliveryOrder(Guid pDeliveryId)
        {
 	        using(VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                Delivery lDelivery =  lContainer.Deliveries.Include("Order.Customer").Where((pDel) => pDel.ExternalDeliveryIdentifier == pDeliveryId).FirstOrDefault();
                return lDelivery.Order;
            }
        }*/

        private Order RetrieveOrder(int orderId)
        {
            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                var order = lContainer.Orders.Include("Customer").Where(x=>x.Id == orderId).FirstOrDefault();
                return order;
            }
        }

    }


}
