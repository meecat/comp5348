﻿using Bank.Services.Interfaces;
using DeliveryCo.Services.Interfaces;
using EmailService.Services.Interfaces;
using MessageBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace VideoStore.Business.Components
{
    public class ExternalServiceFactory
    {
        private static ExternalServiceFactory sFactory = new ExternalServiceFactory();

        public static ExternalServiceFactory Instance
        {
            get
            {
                return sFactory;
            }
        }

        public IEmailService EmailService
        {
            get
            {
                return MessageServiceFactory.GetService<IEmailService>("EmailQueueTransacted");
            }
        }

        public ITransferService TransferService
        {
            get
            {
                 //return GetTcpService<ITransferService>("net.tcp://localhost:9020/TransferService");
               return MessageServiceFactory.GetService<ITransferService>("TransferQueueTransacted");
            }
        }

        public IDeliveryService DeliveryService
        {
            get
            {
                 //return GetTcpService<IDeliveryService>("net.tcp://localhost:9030/DeliveryService");
               return MessageServiceFactory.GetService<IDeliveryService>("DeliveryQueueTransacted"); 
            }
        }
         

        private T GetTcpService<T>(String pAddress)
        {
            NetTcpBinding tcpBinding = new NetTcpBinding() { TransactionFlow = true };
            EndpointAddress address = new EndpointAddress(pAddress);
            return new ChannelFactory<T>(tcpBinding, pAddress).CreateChannel();
        }
    }
}
