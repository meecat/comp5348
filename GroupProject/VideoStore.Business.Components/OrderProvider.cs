﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;
using VideoStore.Business.Entities;
using System.Transactions;
using Microsoft.Practices.ServiceLocation;
using DeliveryCo.MessageTypes;
using Bank.Services.Interfaces;

namespace VideoStore.Business.Components
{
    public class OrderProvider : IOrderProvider, ITransferNotificationService
    {
        public IEmailProvider EmailProvider
        {
            get { return ServiceLocator.Current.GetInstance<IEmailProvider>(); }
        }

        public IUserProvider UserProvider
        {
            get { return ServiceLocator.Current.GetInstance<IUserProvider>(); }
        }

        public void SubmitOrder(Entities.Order pOrder)
        {

            //using (TransactionScope lScope = new TransactionScope())
            //{
            //using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            //{
            try
            {
                LoadMediaStocks(pOrder);
                MarkAppropriateUnchangedAssociations(pOrder);
                pOrder.OrderNumber = Guid.NewGuid();

                if (!pOrder.CheckStockLevels())
                {
                    SendOrderErrorMessage(pOrder, new Exception("Cannot place an order - no more stock for media item"));
                    return;
                }

                //TransferFundsFromCustomer(UserProvider.ReadUserById(pOrder.Customer.Id), pOrder.Total ?? 0.0);
                TransferFundsFromCustomer(UserProvider.ReadUserById(pOrder.Customer.Id), pOrder);

                //pOrder.UpdateStockLevels();

                //PlaceDeliveryForOrder(pOrder);
                //lContainer.Orders.ApplyChanges(pOrder);

                //lContainer.SaveChanges();
                // lScope.Complete();

            }
            catch (Exception lException)
            {
                SendOrderErrorMessage(pOrder, lException);
                throw;
            }
            //}
            //}
            // SendOrderPlacedConfirmation(pOrder);
        }


        public void NotifyTranferCompletion(bool success, string message, Order pOrder)
        {
            Console.WriteLine("got notification from 'TransferNotificationTransacted'.");

            if (!success)
            {
                SendOrderErrorMessage(pOrder, new Exception(message));
                return;
            }

            using (VideoStoreEntityModelContainer lContainer = new VideoStoreEntityModelContainer())
            {
                try
                {
                    LoadMediaStocks(pOrder, lContainer);
                    MarkAppropriateUnchangedAssociations(pOrder);
                    // pOrder.OrderNumber = Guid.NewGuid();

                    pOrder.UpdateStockLevels();
                    lContainer.Orders.ApplyChanges(pOrder);
                    lContainer.SaveChanges();

                    PlaceDeliveryForOrder(pOrder);

                }
                catch (Exception lException)
                {
                    SendOrderErrorMessage(pOrder, lException);
                    throw;
                }
            }
        }

        private void PlaceDeliveryForOrder(Order pOrder)
        {
            Delivery lDelivery = new Delivery() { DeliveryStatus = DeliveryStatus.Submitted, SourceAddress = "Video Store Address", DestinationAddress = pOrder.Customer.Address, Order = pOrder };

            ExternalServiceFactory.Instance.DeliveryService.SubmitDelivery(new DeliveryInfo()
            {
                OrderNumber = lDelivery.Order.OrderNumber.ToString(),
                SourceAddress = lDelivery.SourceAddress,
                DestinationAddress = lDelivery.DestinationAddress,
                DeliveryNotificationAddress = "net.tcp://localhost:9014/DeliveryNotificationTransacted"
            }, pOrder.Id);

        }


        private void MarkAppropriateUnchangedAssociations(Order pOrder)
        {
            pOrder.Customer.MarkAsUnchanged();
            pOrder.Customer.LoginCredential.MarkAsUnchanged();
            foreach (OrderItem lOrder in pOrder.OrderItems)
            {
                lOrder.Media.Stocks.MarkAsUnchanged();
                lOrder.Media.MarkAsUnchanged();
            }
        }

        private void LoadMediaStocks(Order pOrder, VideoStoreEntityModelContainer lContainer = null)
        {
            Action<VideoStoreEntityModelContainer> loadStocks = (container) =>
            {
                foreach (OrderItem lOrder in pOrder.OrderItems)
                {
                    lOrder.Media.Stocks = container.Stocks.Where((pStock) => pStock.Media.Id == lOrder.Media.Id).FirstOrDefault();
                }
            };

            if (lContainer == null)
            {
                using (lContainer = new VideoStoreEntityModelContainer())
                {
                    loadStocks(lContainer);
                }
            }
        }



        private void SendOrderErrorMessage(Order pOrder, Exception pException)
        {
            //bruce: publish error message

            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "There was an error in processsing your order " + pOrder.OrderNumber + ": " + pException.Message + ". Please contact Video Store"
            });
        }

        private void SendOrderPlacedConfirmation(Order pOrder)
        {
            EmailProvider.SendMessage(new EmailMessage()
            {
                ToAddress = pOrder.Customer.Email,
                Message = "Your order " + pOrder.OrderNumber + " has been placed"
            });
        }


        private void TransferFundsFromCustomer(User customer, Order order)
        {
            try
            {
                ExternalServiceFactory.Instance.TransferService.Transfer(order.Total ?? 0.0, customer.BankAccountNumber, RetrieveVideoStoreAccountNumber(), customer.Email, order);
            }
            catch (Exception e)
            {
                throw new Exception("Error Transferring funds for order.");
            }
        }


        private int RetrieveVideoStoreAccountNumber()
        {
            return 123;
        }


    }
}
