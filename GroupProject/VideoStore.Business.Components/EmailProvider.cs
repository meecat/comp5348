﻿using MessageBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VideoStore.Business.Components.Interfaces;

namespace VideoStore.Business.Components
{
    public class EmailProvider : IEmailProvider
    {
        public void SendMessage(EmailMessage pMessage)
        {
            //modified by Bruce 26/05/2017
            //var emailService = MessageServiceFactory.GetService<IEmailService>("EmailQueueTransacted");
            //emailService.SendEmail(new MessageBus.Model.EmailMessage
            //{
            //    Message = pMessage.Message,
            //    ToAddresses = pMessage.ToAddress,
            //    Date = DateTime.Now
            //});


            ExternalServiceFactory.Instance.EmailService.SendEmail
                (
                    new global::EmailService.MessageTypes.EmailMessage()
                    {
                        Message = pMessage.Message,
                        ToAddresses = pMessage.ToAddress,
                        Date = DateTime.Now
                    }
                );
        }
    }
}
