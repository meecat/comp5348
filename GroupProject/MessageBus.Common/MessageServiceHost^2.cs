﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Messaging;

namespace MessageBus
{
    public class MessageServiceHost<TServiceInterface, TServiceType> : IDisposable
        where TServiceType : TServiceInterface
    {
        private ServiceHost mHost;

        public MessageServiceHost(String pAddress, String pMexAddress, bool pCreateQueueIfNotExist = false, String pQueueAddress = null)
        {
            if (pCreateQueueIfNotExist && (pQueueAddress != null))
            {
                CreateQueueIfNotExist(pQueueAddress);
            }

            mHost = new ServiceHost(typeof(TServiceType));

            if (!mHost.Description.Behaviors.Contains(typeof(ServiceMetadataBehavior)))
            {
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                mHost.Description.Behaviors.Add(smb);
            }

            mHost.AddServiceEndpoint(typeof(TServiceInterface),
                new NetMsmqBinding(NetMsmqSecurityMode.None) { Durable = true }, pAddress);

            mHost.AddServiceEndpoint(typeof(IMetadataExchange),
                MetadataExchangeBindings.CreateMexTcpBinding(),
                pMexAddress);

            mHost.Open();
        }

        private void CreateQueueIfNotExist(String pQueueAddress)
        {
            // Create the transacted MSMQ queue if necessary.
            if (!MessageQueue.Exists(pQueueAddress))
                MessageQueue.Create(pQueueAddress, true);
        }

        public void Dispose()
        {
            if (mHost != null)
            {
                mHost.Close();
            }
        }
    }
}
