﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace MessageBus
{
    public class MessageServiceFactory
    {
        public static T GetService<T>(String mqName)
        {
            return GetChannelFactory<T>(mqName).CreateChannel();
        }

        private static ChannelFactory<T> GetChannelFactory<T>(string mqName)
        {
            var mqAddress = "net.msmq://localhost/private/" + mqName;

            var lBinding = new NetMsmqBinding(NetMsmqSecurityMode.None)
            {
                ExactlyOnce = true,
                Durable = true
            };

            var myEndpoint = new EndpointAddress(mqAddress);

            var myChannelFactory = new ChannelFactory<T>(lBinding, myEndpoint);
           
            return myChannelFactory;
        }
    }
}
