﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using VideoStore.Business.Entities;

namespace Bank.Services.Interfaces
{
    [ServiceContract]
    public interface ITransferService
    {
        //[OperationContract]
       // [TransactionFlow(TransactionFlowOption.Allowed)]
        [OperationContract(IsOneWay = true, Action = "*")]
        void Transfer(double pAmount, int pFromAcctNumber, int pToAcctNumber, string customerEmail, Order order);
    }
}
