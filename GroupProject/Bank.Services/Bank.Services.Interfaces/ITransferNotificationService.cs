﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using VideoStore.Business.Entities;

namespace Bank.Services.Interfaces
{
    [ServiceContract]
    public interface ITransferNotificationService
    {
        [OperationContract(IsOneWay = true, Action = "*")]
        void NotifyTranferCompletion(bool success, string message, Order order);
    }
}
