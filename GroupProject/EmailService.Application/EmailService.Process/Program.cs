﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using MessageBus;
using EmailService.Services.Interfaces;

namespace EmailService.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            ResolveDependencies();

            var cAddress = "net.msmq://localhost/private/EmailQueueTransacted";
            var cMexAddress = "net.tcp://localhost:9012/EmailQueueTransacted/mex";

            using (var lHost = new MessageServiceHost<IEmailService, EmailService.Services.EmailService>(
                cAddress, cMexAddress, true, ".\\private$\\EmailQueueTransacted"))
            {
                Console.WriteLine("MSMQ - EmailService Host Started. Press Q to quit.");
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }

            //using (ServiceHost lHost = new ServiceHost(typeof(EmailService.Services.EmailService)))
            //{
            //    lHost.Open();
            //    Console.WriteLine("Email Service Started");
            //    while (Console.ReadKey().Key != ConsoleKey.Q) ;
            //}
        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
    }
}
