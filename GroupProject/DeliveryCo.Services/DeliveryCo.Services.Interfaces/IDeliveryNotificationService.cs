﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace DeliveryCo.Services.Interfaces
{
    public enum DeliveryInfoStatus { Submitted, Delivered, Failed }

    [ServiceContract]
    public interface IDeliveryNotificationService
    {
        //[OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [OperationContract(IsOneWay = true, Action = "*")]
        void NotifyDeliveryCompletion(Guid pDeliveryId, int orderId, DeliveryInfoStatus status);
    }

    //[ServiceContract]
    //public interface IOrderNotificationService
    //{ 
    //    [TransactionFlow(TransactionFlowOption.Allowed)]
    //    [OperationContract(IsOneWay = true, Action = "*")]
    //    void NotifyOrderCompletion(Guid pDeliveryId, int orderId);
    //}
}
