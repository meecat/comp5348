﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DeliveryCo.MessageTypes;
using VideoStore.Business.Entities;

namespace DeliveryCo.Services.Interfaces
{
    [ServiceContract]
    public interface IDeliveryService
    { 
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [OperationContract(IsOneWay = true, Action = "*")]
        //[OperationContract]
        void SubmitDelivery(DeliveryInfo pDeliveryInfo, int orderId);
    }
}
