﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bank.Services.Interfaces;
using Bank.Business.Components.Interfaces;
using System.ServiceModel;
using Microsoft.Practices.ServiceLocation;
using EmailService.Services.Interfaces;
using MessageBus;
using VideoStore.Business.Entities;

namespace Bank.Services
{
    public class TransferService : ITransferService
    {
        private ITransferProvider TransferProvider
        {
            get { return ServiceLocator.Current.GetInstance<ITransferProvider>(); }
        }
         
       // [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public void Transfer(double pAmount, int pFromAcctNumber, int pToAcctNumber, string customerEmail, Order order)
        {
            var transferNotificationService = MessageServiceFactory.GetService<ITransferNotificationService>("TransferNotificationTransacted");
            try
            {
                TransferProvider.Transfer(pAmount, pFromAcctNumber, pToAcctNumber);
                Console.WriteLine("{3}: tranfer ${0} from acount '{1}' to account '{2}'", pAmount, pFromAcctNumber, pToAcctNumber, DateTime.Now);
                transferNotificationService.NotifyTranferCompletion(true, "Success", order);
            }
            catch (Exception e)
            {
                transferNotificationService.NotifyTranferCompletion(false, e.Message, order);
            }
        }
    }
}
