﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DeliveryCo.Services;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.ServiceLocatorAdapter;
using Microsoft.Practices.ServiceLocation;
using System.Configuration;
using MessageBus;
using DeliveryCo.Services.Interfaces;

namespace DeliveryCo.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            ResolveDependencies();

            var cAddress = "net.msmq://localhost/private/DeliveryQueueTransacted";
            var cMexAddress = "net.tcp://localhost:9040/DeliveryQueueTransacted/mex";
            using (var mHost = new MessageServiceHost<IDeliveryService, DeliveryService>(
                    cAddress, cMexAddress, true, ".\\private$\\DeliveryQueueTransacted"))
            {
                Console.WriteLine("MSMQ - Delivery Service Host Started. Press Q to quit.");
                while (Console.ReadKey().Key != ConsoleKey.Q) ;
            }
             
            //using (ServiceHost lHost = new ServiceHost(typeof(DeliveryService)))
            //{
            //    lHost.Open();
            //    Console.WriteLine("Delivery Service started. Press Q to quit");
            //    while (Console.ReadKey().Key != ConsoleKey.Q) ;
            //}
        }

        private static void ResolveDependencies()
        {

            UnityContainer lContainer = new UnityContainer();
            UnityConfigurationSection lSection
                    = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            lSection.Containers["containerOne"].Configure(lContainer);
            UnityServiceLocator locator = new UnityServiceLocator(lContainer);
            ServiceLocator.SetLocatorProvider(() => locator);
        }
    }
}
